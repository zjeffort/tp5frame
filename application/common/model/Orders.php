<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class Orders
 * 订单
 * @package app\common\model
 */
class Orders extends Model{
    public $table = 'orders';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    //获取器
    public function getImagesAttr($value)
    {
        return json_decode($value, true);
    }

    //异地查档；拥有资料
    public function getFileNowDataAttr($value)
    {
        $arr = json_decode($value, true);
        if( !empty($arr) ){
            $res = db('now_data')->where('id','in', implode(',',$arr))->select();
        }
        return $res ?? [];
    }

    public function getConsultPAttr($value)
    {
        $res = Db::table('region')->where('code',$value)->find();
        return $res['name'];
    }

    public function getConsultCAttr($value)
    {
        $res = Db::table('region')->where('code',$value)->find();
        return $res['name'];
    }

    public function getStatusAttr($value)
    {
        $status = ['0'=>'待支付','1'=>'应征中','2'=>'进行中','3'=>'已完成','4'=>'已撤销'];
        return $status[$value];
    }

    public function getUserIdAttr($value)
    {
        $userInfo = Db::table('users')->where('id', $value)->find();
//        return $userInfo['mobile'];
        return $userInfo;
    }

    public function getLawIdAttr($value)
    {
        if(!$value) return $value;
        if(!$info = model('Lawyer')->where('id', $value)->find()){
            return $value;
        }
        return $info->toArray();
    }

    public function getTypeIdAttr($value){
        $res = Db::table('order_type')->where('id', $value)->find();
        return $res['name'];
    }

    public function getEarnestMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

    public function getPaymentAttr($value)
    {
        return sprintf("%.2f", $value);
    }

    public function getTotalPaymentAttr($value)
    {
        return sprintf("%.2f", $value);
    }

    public function getCencelMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

    public function getSettlementMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }


//    获取订单类型，1，2级分类的名称
//    public function getTypeIdAttr($value)
//    {
//        $orderType = Db::table('order_type')->where('id', $value)->find();
//        $res = '';
////        switch ($orderType['order_type']){
////            case 2: //异地查档
////                $res = $this->clevel($orderType['pid'], 'query_type').$orderType['name'];
////                break;
////            default:
////                $res = $this->clevel($orderType['pid']).$orderType['name'];
////                break;
////        }
////        $res = $this->clevel($orderType['pid']).$orderType['name'];
//
//        return $res;
//    }

    //获取订单类型
    public function getOrderType($typeId)
    {
//        switch ($type){
//            case 2:     //异地查档
//                $orderType = Db::table('query_type')->where('id', $typeId)->find();
//                $dbName = 'query_type';
//                break;
//            case 3:
//                break;
//            default:
//                $orderType = Db::table('order_type')->where('id', $typeId)->find();
//                $dbName = 'order_type';
//                break;
//        }
        $orderType = Db::table('order_type')->where('id', $typeId)->find();
        return ($this->clevel($orderType['pid']).$orderType['name']);
    }

    //无限递归，反查
    public function clevel($pid)
    {
        if( $pid > 0 ){
            $p = Db::table('order_type')->where('id', $pid)->find();
            $res = $p['name'].'->';
            if($p['pid'] > 0){
                $res = $this->clevel($p['pid']).$res;
            }
            return $res;
        }
    }

}
