<?php
namespace app\index\behavior;

class UserCheck
{
    use \traits\controller\Jump;//类里面引入jump类

    //绑定到CheckAuth标签，可以用于检测Session以用来判断用户是否登录
    public function run(&$params){
        $uid = session("user");
        if(!isset($uid)){
            $uid = "";
        }
        if($uid == null || $uid == "" || $uid == "null" || $uid == 0){
            return $this->error('请登录！','user/login', 1);
        }
    }
}
