<?php
namespace Pay;

use think\Exception;
use think\Session;
use Pay\wx\example\Pay;

/**
 * Class PayCenter
 * @package Pay
 * 未使用该类！！！！
 */
class PayCenter
{
    public $result = '';
    //判断什么支付，调用对应的支付方式
    public function __construct($data)
    {
        switch ($data['trade_type'])
        {
            case 'NATIVE':
            case 'JSAPI':
                $data = [
                    'trade_type'=>$data['trade_type'],
                    'body'=>$data['body'],
                    'trade_no'=>$data['trade_no'],
                    'total_fee'=>$data['total_fee'],
                    'notify_url'=>$data['notify_url'],
                ];
                $this->result = new Pay($data);
                break;
            case 'alipay':
                break;
        }
        if($this->result) return $this->result;
        return false;
    }

//    //支付回调
//    public function payCallBack()
//    {
//
//    }

}