<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class OrdersScore
 * 订单评价
 * @package app\common\model
 */
class OrdersScore extends Model{
    public $table = 'order_score';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    //获取器
    public function getImagesAttr($value)
    {
        return json_decode($value, true);
    }

    public function getUserIdAttr($value)
    {
        $userInfo = model('UsersS')->where('id', $value)->find();
        return $userInfo;
    }

}
