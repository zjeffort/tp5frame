<?php
namespace app\common\validate;

use think\Validate;

class Orders extends Validate{
    //规则
    protected $rule = [
        "type_id|类型（类目）或领域"        =>  "require|token",
        "__token__"   =>  "require",
        "content|需求(案件)详情"   =>  "require",
        "file_category_id|查档类目"   =>  "require",
        "file_now_data|现有资料"   =>  "require",
        "order_time|订单时效"   =>  "require",
        "tel_name|联系人"   =>  "require",
        "tel|联系电话"   =>  "require|regex:/^1[34578]{1}\d{9}$/",
        "rece_addr|收货地址"   =>  "require",
        "consult_p|省"=>"require",
        "consult_c|市"=>"require",
        "consult_a|区"=>"require",
        //案件协助
        'bde|标的额'=>"require|between:0.01,9999999",
        'yaf|案源费'=>"require|between:0.01,9999999",
     ];

     protected $message =   [
         "type_id.token"      =>  "非法操作，不能重复提交",
     ];

     //场景
    protected $scene    =   [
        //咨询类
        'zxfw'=>[
            'type_id','__token__','content',"consult_p","consult_c","consult_a"
        ],
        //异地查档
        'ydcd'=>[
            'type_id','__token__','file_now_data',"consult_p","consult_c","consult_a",
            "order_time","tel_name","tel","rece_addr"
        ],
        //一键响应
        'yjxy'=>[
            '__token__','content',"consult_p","consult_c","consult_a"
        ],
        //案件协助
        'ajxz'=>[
            'type_id','__token__',"consult_p","consult_c","consult_a",
            'tel_name','tel','bde',"yaf","content"
        ],
        'queryFile'=>[
            'type_id','__token__','file_type_id','file_category_id',
            'file_now_data','order_time','tel_name','tel','rece_addr'
        ],
        'createOrder'=>[
            'type_id','content','__token__'
        ]
    ];
}