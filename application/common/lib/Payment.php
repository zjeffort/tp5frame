<?php
namespace app\common\lib;

/**
 * Class PayCallBack
 * 支付
 * @package app\common\lib
 */
class Payment{
    // 初始化签权对象
    public function __construct(){}

    static public function pay($params)
    {
        switch ($params['trade_type'])
        {
            case 'NATIVE':
            case 'JSAPI':
                $data = [
                    'trade_type'=>$params['trade_type'],
                    'body'=>$params['body'],
                    'trade_no'=>$params['trade_no'],
                    'total_fee'=>$params['total_fee'] * 100,
                    'notify_url'=>'http://'.$_SERVER['HTTP_HOST'].url('Pay/notify'),
//                    'notify_url'=>'http://test1.qwas.net',
                    'attach'=>$params['attach']??'',        //回调时候，回传参数。
                    'openid'=>$params['openid']??''
                ];
                $res = (array)(new \Pay\wxpay\example\Pay($data));
                return self::wxPay($res['result'],$params);
                break;
            case 'alipay':
                //支付宝支付
                $data = [
                    'body'=>trim($params['body']),                  //商品描述，可空
                    'subject'=>trim($params['subject']),            //订单名称，必填
                    'total_amount'=>trim($params['total_amount']),  //付款金额，必填
                    'out_trade_no'=>trim($params['out_trade_no']),  //商户订单号，商户网站订单系统中唯一订单号，必填
                    'passback_params'=>urlencode( http_build_query($params['passback_params']) ),  //公用回传参数，可空
//                    'passback_params'=>urlencode('law_id=1&user_id=2'),  //商户订单号，商户网站订单系统中唯一订单号，必填
                ];

                return (new \Pay\alipay\lib\Pagepay())->pay($data);

                break;
        }
    }

    //微信支付
    static private function wxPay($res, $params)
    {
        if($res['result_code'] != 'SUCCESS' || $res['return_code'] != 'SUCCESS'){
            mylog(['desc'=>'微信支付失败','msg'=>$res['err_code_des'],'data'=>['request'=>$params, 'response'=>$res]], 'pay');
//            return $this->returnJson('',1,'生成支付失败，请刷新重试或与管理员联系');
            return [];
        }

        //NATIVE支付
        if ($params['trade_type'] == 'NATIVE'){
            //todo 生成的二维码图片，是否需要删除？
            return ['code_url'=>createQRcode($res['code_url'])];  //return "/qrcode/c116d6ee02e9b77b3c35f42102aa588b.png"
        } elseif ( $params['trade_type'] == 'JSAPI' ){ //JSAPI支付
            $tools = new \Pay\wxpay\example\WxPayJsApiPay();
            //调用微信JS api 支付
            $jsApiParameters = $tools->GetJsApiParameters($res);
            //获取共享收货地址js函数参数
            $editAddress = $tools->GetEditAddressParameters();
            return ['jsApiParameters'=>$jsApiParameters, 'editAddress'=>$editAddress];
        }
    }
}
