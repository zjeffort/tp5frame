<?php
namespace app\common\lib;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

/**
 * Class Qiniu
 * 七牛云封装
 * @package app\common\lib
 */
class Qiniu{

    public $accessKey = '';
    public $secretKey = '';
    public $bucket = '';
    public $auth = '';

    // 初始化签权对象

    public function __construct()
    {
        //获取七牛云配置
        if(!$qiniuConf = get_A_conf('qiniu','qiniu')){
            return false;
        }

        $this->accessKey = $qiniuConf['AccessKey'];
        $this->secretKey = $qiniuConf['SecretKey'];
        $this->bucket = $qiniuConf['bucket'];
        $this->domain = $qiniuConf['domain'];
        //--------------自定义调整,字段-------------
        $this->videoSize = $qiniuConf['videoSize'];
        $this->audioSize = $qiniuConf['audioSize'];

        $this->auth = new Auth($this->accessKey, $this->secretKey);
    }

    // 生成上传Token
    public function getToken()
    {
        //此数组，是设置回调级一些上传策略。常用的就是 callbackUrl、 callbackBody
        $putPolicy = [
//            "callbackUrl"=>"http://www.qwas.net/index.php",
//            "callbackBody"=>"key=$(key)&fname=$(fname)&fsize=$(fsize)&mimeType=$(mimeType)",//七牛云特定的写法,表示回调函数接收的参数
//            "callbackBodyType"=>"application/json",
//            'insertOnly' => 0,            //这里不能用true
//            'fsizeLimit' => 500,     //50M
//            'mimeLimit' =>'image/*',
//            'returnBody' => json_encode([ //这里也要编码一次，returnBody的值整体是一个json字符串也可以
//                'name' => '$(fname)',       //原始文件名
//                'size' => '$(fsize)',
//                'w' => '$(imageInfo.width)',
//                'h' => '$(imageInfo.height)',
//                'hash' => '$(etag)',
//                'key' => 'a/b/c/'.md5(time()),          //上传的时候的文件名
//                'mimeType' => '$(mimeType)',//mime类型
//            ])
        ];
        return $this->auth->uploadToken($this->bucket,null, 300,$putPolicy);
    }

    //删除资源
    public function delFile($delFileName)
    {
        // 管理资源
        $bucketManager = new \Qiniu\Storage\BucketManager($this->auth);

        // 删除文件操作
        $res = $bucketManager->delete($this->bucket, $delFileName);

        return $res;
    }

    // 生成下载Token
    public function getDownToken($url)
    {
        return $this->auth->privateDownloadUrl($url);
    }

    public function callbackRes($params){
        //需要的参数，用户id，文件大小fsize，类型mimeType。
          $userId = substr($params['key'],0, strpos($params['key'], '/'));
          $fSize = $params['fsize'];
          $fType = substr($params['mimeType'],0, strpos($params['mimeType'], '/'));
          if($fType){

          }

//        [key] => 996/c21f969b5f03d33d43e04f8f136e7682.png
//        [fname] => default.png
//        [fsize] => 428
//        [mimeType] => image/png   video/mp4
    }
}
