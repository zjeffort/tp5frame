<?php
namespace app\common\model;

use think\Model;
use think\Db;
use app\common\lib\Consult;

/**
 * Class Lawyer
 * 律师信息
 * @package app\common\model
 */
class Lawyer extends Model{
    public $table = 'lawyer';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    public function getConsultPAttr($value)
    {
        return Consult::ConsultName($value);
    }

    public function getConsultCAttr($value)
    {
        return Consult::ConsultName($value);
    }

    public function getStatusAttr($value)
    {
        $status = ['0'=>'等待审核','1'=>'审核通过','2'=>'证件到期','3'=>'审核不通过'];//0:等待审核，1:审核通过，2:证件到期，3:保留字段
        return $status[$value];
    }

    public function getLevelAttr($value)
    {
        if(is_numeric($value) !== true ) return ;
        $status = ['0'=>'未签约','1'=>'已签约'];
        return $status[$value];
    }

    public function getUserIdAttr($value)
    {
        $info = db('users')->where('id', $value)->find();
        return $info;
    }

    public function getSpecialityAttr($value)
    {
        return json_decode($value, true);
    }

}
