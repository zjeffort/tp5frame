<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class OrdersEnlist
 * 订单应征
 * @package app\common\model
 */
class OrdersEnlist extends Model{
    public $table = 'order_enlist';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    //获取器
    public function getLawIdAttr($value)
    {
        $res = \app\common\lib\LawInfo::getLawInfo($value);
        //接单量
        $res['orderNum'] = db('orders')->where('law_id',$res['id'])->where('status', 'in', '2,3')->count();
        //评分
        $res['score'] = sprintf("%.1f", db('order_score')->where('law_id',$res['id'])->avg('score'));
        //地区
        $res['consult'] = \app\common\lib\Consult::ConsultName($res['consult_p']).'-'.\app\common\lib\Consult::ConsultName($res['consult_c']);
        return $res;
    }

    public function getMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}
