<?php
namespace app\index\controller;

use app\common\lib\LawInfo;
use think\Exception;
use think\Request;
use think\Db;
use app\common\lib\PayCallBack;
use app\common\lib\Payment;
/**
 * Class Pay
 * @packageName 支付
 */
class Pay extends Base
{
    public function _initialize()
    {
        parent::_initialize();
    }

    //订单支付页
    public function index(){
        return $this->fetch();
    }

    //支付
    public function toPay()
    {
        if(!request()->isAjax()){
            return $this->returnJson([], 1, '非法请求');
        }

        $params = request()->param();
        $where = [];
        $orderInfo = model('Orders')->where($where)->find();

        switch ($params['order_type']){
            case 1:
                $body = '订单定金';
                $trade_no           = $orderInfo['id'];
                $total_amount       = $orderInfo['earnest_money'];
                $backs['order_id']  = $orderInfo['id'];
                $backs['type']  = 1;
                break;
            case 2:
                $body = '订单尾款';
                //...........
                //...........
                //...........
                break;
        }

        //数据组合
        $data['body'] = $body;                              //商品描述
        $data['trade_type'] = $params['trade_type'];        //支付类型

        //选择的支付方式alipay,wxpay
        $total_amount = abs($total_amount);                 //取绝对值
        if($params['trade_type'] == 'alipay'){
            $scene = 'alipay';
            $data['subject']            = $body;            //订单名称
            $data['total_amount']       = $total_amount;    //付款金额
            $data['out_trade_no']       = $params['order_type'] == 2 ? $trade_no.'s' : $trade_no;        //商户订单号
            $data['passback_params']    = $backs;           //回传参数
        }else{
            $scene                      = 'wxpay';
            $data['trade_no']           = $trade_no;
            $data['total_fee']          = $total_amount;
            $data['attach']             = $backs;           //回传参数
        }

        //数据验证
        $validate = validate("Payment");
        if(!$validate->scene($scene)->check($data)){
            return $this->returnJson('',1,$validate->getError());
        }

        //统一支付入口
        $res = Payment::pay($data);

        if($scene == 'wxpay'){
            if( !$res ){
                $this->returnJson('',1,'生成支付失败，请刷新重试或与管理员联系');
            }
            return $this->returnJson($res);
        }

    }

    //支付回调
    public function notify()
    {
        $params = request()->except(['/'.request()->path(), 'file']);
        if(!$params){
            $res = file_get_contents("php://input");
            $params = json_decode(json_encode( simplexml_load_string($res, 'SimpleXMLElement', LIBXML_NOCDATA) ), true);
        }

        if( PayCallBack::notify($params) ){
            $trade_type = '';   //用于callbackEcho识别
            /*
             * 调用回调方法的时候已经做过数据验证，判断过了。此处不需要判断，只需做识别,调用对应的方法即可
             */
            $data = [];
            if($params['trade_status']??''){        //alipay
                parse_str(urldecode($params['passback_params']), $parse_strs);

            }elseif($params['result_code']){    //wxpay
                $attach = json_decode($params['attach'], true);              //公用回传参数
            }

            echo $this->callbackEcho($trade_type,'succ');
        }else{
            echo $this->callbackEcho($trade_type??'');
        }
    }

    //回复通知
    private function callbackEcho($trade_type,$res='fail'){
        if($trade_type == 'alipay'){
            if($res == 'succ'){
                echo 'success';
            }else{
                echo 'fail';
            }
        }else{
            $values = [];
            if($res == 'succ'){
                $values = [
                    'return_code' => 'SUCCESS', //FAIL
                    'return_msg' => 'OK',       //失败就返回失败的文字
                ];
            }else{
                $values = [
                    'return_code' => 'FAIL',
                    'return_msg' => '失败',
                ];
            }

            $xml = "<xml>";
            foreach ($values as $key=>$val)
            {
                if (is_numeric($val)){
                    $xml.="<".$key.">".$val."</".$key.">";
                }else{
                    $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
                }
            }
            $xml.="</xml>";

            echo $xml;
        }
    }

    //微信轮询
    public function ajaxFindOrderPayRes()
    {
        if(request()->isAjax()){
            $params = input();
            //查询逻辑
            $res = model()->where()->find();

            if($res) return $this->returnJson();
        }
    }

}
