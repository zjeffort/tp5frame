<?php
namespace app\testcase\controller;

use think\Request;
use think\Db;

class Qiniu extends Base
{
    public $mdl = '';
    public function _initialize()
    {
        parent::_initialize();
        $this->mdl = model("LawColumn");
    }

    //列表
    public function index(){
        return $this->fetch();
    }

    /**
     * 获取七牛云上传Token
     */
    public function ajaxQn(){
        $qn = new \app\common\lib\Qiniu();

        $params = [
            'uptoken'=>$qn->getToken(),
            'domain'=>$qn->domain,
            //↓↓↓↓↓↓↓↓↓↓↓↓↓自定义参数↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            'videoSize'=>$qn->videoSize,
            'type'=>'mp4'       //上传文件格式，用于前端页面，进行判断。
        ];

        echo json_encode($params);
    }

    /**
     * 删除资源
     * @param $delFileName 传入md5加密的文件名称.jpg
     */
    public function ajaxQnDel()
    {
        $delFileName = input('fileName');

        $qn = new \app\common\lib\Qiniu();

        $res = $qn->delFile($delFileName);

        $returnArr = [
            "err"=>"0",
            "msg"=>"成功",
            "data"=>[],
        ];
        if (is_null($res)) {
            //删除成功，修改表字段
            $data = [
                "id"=>input('id'),
                "mp4"=>'',
                "u_time"=>time()
            ];
            $res = $this->mdl->isUpdate(true)->save($data);
            // 为null成功
            // return true;
            echo json_encode($returnArr);
        }else{
            echo json_encode([
                "err"=>"1",
                "msg"=>"删除失败",
                "data"=>[],
            ]);
        }
    }
}
