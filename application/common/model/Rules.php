<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class Rules
 * @packageName 关于我们->平台规则Model
 */
class Rules extends Model{
    public $table = 'rules';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;
}
