<?php
namespace app\admin\validate;

use think\Validate;

/**
 * Class About
 * @package 关于我们验证器
 */
class About extends Validate{
    //规则
    protected $rule = [
        "title|标题" => "require",
        "desc|描述" => "require",
        "content|内容" =>  "require",
        "rule_content|平台规则" =>  "require",
     ];

     protected $message =   [
     ];

     //场景
    protected $scene    =   [
        "rules" =>  [
            "rule_content"
        ],
        "problem" =>  [
            "title","desc","content"
        ],
    ];
}