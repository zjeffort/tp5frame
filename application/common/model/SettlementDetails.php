<?php
namespace app\common\model;

use think\Model;

/**
 * Class SettlementDetails
 * 消费明细
 * @package app\common\model
 */
class SettlementDetails extends Model{
    public $table = 'settlement_details';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    public function getStatusAttr($value)
    {
        $status = [
            '1'=>'购买用户vip','2'=>'购买律师vip','3'=>'支付订单定金','4'=>'支付订单尾款',
            '6'=>'订单结算','7'=>'推荐注册奖励','8'=>'推荐注册律师奖励','9'=>'上传合同','10'=>'提现申请','11'=>'提现完成','12'=>'撤单退款'
        ];
        return $status[$value];
    }

    public function getUserIdAttr($value)
    {
        $userInfo = db('users')->where('id', $value)->find();
        return $userInfo;
    }

    public function getSourceAttr($value)
    {
        return json_decode($value, true);
    }

    public function getMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}
