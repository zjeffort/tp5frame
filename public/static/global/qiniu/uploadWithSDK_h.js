//验证MP4
function fileCheck(file, size){
    var fileName = file.name;
    var extStart = fileName.lastIndexOf(".");
    var ext = fileName.substring(extStart,fileName.length).toUpperCase();    //toUpperCase  :  方法用于把字符串转换为大写
    //判断格式
    if(ext !== '.MP4'){
        alert(fileName + '格式不正确');
        return false;
    }
    //判断大小
    if(ext == '.MP4'){
        if(file.size > (size * (1024*1024)) ){
            alert(fileName + '文件不能大于' + size  +'MB');
            return false;
        }else{
            return true;
        }
    }
}

//上传
function uploadWithSDK(file, token, putExtra, config, domain) {
    var finishedAttr = [];
    var compareChunks = [];
    var observable;

    if (file) {
        var key = md5(file.name.split(".")[0] + Date.parse(new Date())) + '.' + file.name.split(".")[1];

        //todo 获取进度条节点
        //todo 需要加，在页面中显示上传中
        var error = function(err) {
            // board.start = true;
            // $(board).find(".control-upload").text("继续上传");
            layer.alert('上传失败！');
            return ;
        };

        var complete = function(res) {

            var url = domain + '/' + key;
            //查看资源
            $('.btn-success').attr('href', url);
            //input值
            $('#mp4').val(key);

            // console.log(url);
            //获取上传视频时长
            $("#myvide_time").prop("src", url);
            $("#myvide_time")[0].addEventListener("loadedmetadata", function () {
                var mp4_time = parseInt(this.duration); //获取总时长
                $("#mp4_time").val(mp4_time);
            });

            // console.log(res);
            layer.alert('上传成功！');
            //todo 在页面中显示上传完成
            // left: 185px;
            $('.btn-success').removeClass('hidden');
            $('.btn-danger').removeClass('hidden');
            $('#selectFile').css('left','185px');
            return ;
        };

        var next = function(response) {
            var total = response.total;
            var jindu=parseInt(total.percent);
            //进度条加载
            $('.layui-progress-bar').css({"width":jindu+'%'});
            $('.layui-progress-text').html(jindu+'%');
        };

        var subObject = {
            next: next,
            error: error,
            complete: complete
        };
        // 调用sdk上传接口
        observable = qiniu.upload(file, key, token, putExtra, config);
        //上传
        observable.subscribe(subObject);
    }
}