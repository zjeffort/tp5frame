<?php
namespace app\common\lib;

use think\Db;

/**
 * Class SeeUserInfo
 * 用户相关信息
 * @package app\common\lib
 */
class SeeUserInfo{
    public function __construct()
    {
    }

    //基本信息
    static public function own($userId){
        $where  =   [
            "id"    =>  $userId,
        ];
        $user = model("ClientUsers")
            ->where($where)
            ->find();
        return $user;
    }

    //团队成员
    static public function team($userId)
    {
        //为我服务过的律师
        $where  =   [
            "user_id"    =>  $userId,
        ];
        $teamLaw = db("orders")
            ->where($where)
            ->field('law_id')
            ->select();

        //律师id，去重
        $lawIds = array_unique(array_column($teamLaw, 'law_id'));
        //获取律师信息
        $res = model("Lawyer")->where('id', 'in', $lawIds)->paginate();

        return $res;

    }

    //签约律师
    static public function signLaw($userId){
        $where  =   [
            "user_id"    =>  $userId,
        ];
        $signLaw = model("SignLaw")
            ->where($where)
            ->field('law_id')
            ->select();

        //用去重的方法，把数组变成索引数组
        $lawIds = array_unique(array_column($signLaw, 'law_id'));

        $res = model("Lawyer")->where('id', 'in', $lawIds)->paginate();

        return $res;
    }

    //发布订单列表 todo 不需要了
    static public function orderList()
    {
        //获取用户发布的所有订单
        if(!input('t') || input('t') == 'user'){
            //普通订单
            $ordersInfo = model('Orders')->where('user_id',input('id'))->paginate();
            foreach ($ordersInfo as $key=>&$val){
                $val->type_name = model('Orders')->getOrderType($val->type_id);
            }
            return $ordersInfo;
        }else{
            //律师订单
        }
    }

    //卡号信息
    static public function card($userId)
    {
        $Crad = model('Card')->where('user_id',$userId)->paginate();
        return $Crad;
    }

    //消费明细
    static public function consume($userId)
    {
        $SettlementDetails = model('SettlementDetails')->where('user_id',$userId)->paginate();
        return $SettlementDetails;

    }

    //推荐详情
    static public function recommend($userId)
    {
        $recommendInfo = model('Recommend')->where('user_id',$userId)->paginate();
        return $recommendInfo;
    }


}
