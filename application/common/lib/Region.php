<?php
namespace app\common\lib;

use think\Db;

/**
 * Class Region
 * 地区三级联动
 * @package app\common\lib
 */
class Region{
    //省
    static public function province(){
        return Db::table('region')->where('pid','0')->select();
    }
    //市
    static public function city($typeId){
        return Db::table('region')->where('pid',$typeId)->select();
    }
    //区
    static public function area($typeId){
        return Db::table('region')->where('pid',$typeId)->select();
    }

    //获取地址二级联动
    static public function getRegion($data)
    {
        if(isset($data['p']) && $data['p']){//市
            $res = Region::city($data['p']);
        }else{
            return false;
        }
        return $res;
    }
}
