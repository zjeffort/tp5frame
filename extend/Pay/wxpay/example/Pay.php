<?php
namespace Pay\wxpay\example;

use Pay\wxpay\lib\WxPayApi;

class Pay extends WxPayBase
{
    public $result = '';

    public function __construct($data)
    {
        parent::__construct();
        self::$params->SetBody($data['body']);                              //商品简单描述
        self::$params->SetOut_trade_no($data['trade_no'].date("YmdHis"));   //商户订单号: 10 + order_id + date("YmdHis")
        self::$params->SetTotal_fee($data['total_fee']);                    //订单总金额，单位为分
        self::$params->SetNotify_url($data['notify_url']);                  //回调url
        self::$params->SetTrade_type($data['trade_type']);                          //调用支付类型
        self::$params->SetAttach(json_encode($data['attach']));                              //公用回传参数，可空

        if($data['trade_type'] == 'NATIVE'){
            //trade_type=NATIVE时，此参数必传。此参数为二维码中包含的商品ID，商户自行定义。
            self::$params->SetProduct_id($data['trade_no']);  //order_id
        }
        if($data['trade_type'] == 'JSAPI'){
            //trade_type=JSAPI时，此参数必传，此参数为微信用户在商户对应appid下的唯一标识
            //todo 微信端用户登录的时候，就已经记录了用户的openid，直接获取即可。
//            $base = new \app\wap\controller\Base();
//            $userInfo = $base::$userInfo;
//            session("user")['wechat_openid'];
            self::$params->SetOpenid($data['openid']);
        }

        $this->result = WxPayApi::unifiedOrder(self::$conf, self::$params);      //统一下单
        /**
         * 如果是NATIVE：取值为,result['code_url']
         */
    }

}
