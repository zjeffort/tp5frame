<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class Contract
 * 订单合同
 * @package app\common\model
 */
class Contract extends Model{
    public $table = 'contract';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    public function getUserIdAttr($value, $data)
    {
        //2表示律师,1表示用户
        if($data['role'] == 2){
            $info = model('Lawyer')->where('user_id', $value)->find()->toArray();
        }else{
            $info = db('users')->where('id', $value)->find();
        }
        return $info;
    }

    public function getMoneyAttr($value)
    {
        return (float)$value;
    }

    public function getPayMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

//    public function getStatusAttr($value)
//    {
//        $status = ['1'=>'待奖励','2'=>'已奖励'];
//        return $status[$value];
//    }
//
//    public function getRoleAttr($value)
//    {
//        $status = ['1'=>'用户','2'=>'律师'];
//        return $status[$value];
//    }

}
