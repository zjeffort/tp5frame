<?php
namespace app\common\model;

use think\Model;

/**
 * Class Banner
 * 未使用，该类。使用全局配置替代了
 * @package app\common\model
 */
class Banner extends Model{
    public $table = 'banners';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;
}
