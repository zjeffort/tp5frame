<?php
namespace app\common\model;

use think\Model;

/**
 * Class QueryType
 * 未使用，该类。
 * @package app\common\model
 */
class QueryType extends Model{
    public $table = 'query_type';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

}
