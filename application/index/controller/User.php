<?php
namespace app\index\controller;

use think\Db;
use think\Request;
use app\common\lib\UserCheck;
use think\Hook;
use Aliyun\SendSms;

/**
 * Class User
 * 登录、注册、修改密码、忘记密码、发送验证
 * @package app\index\controller
 */
class User extends Base
{
    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * ajax发送手机短信验证码
     */
    public function sendCode(){
        $mobile = input('post.mobile');
        $smsType = input('post.smsType');
        if($mobile && $smsType){
            $res = model('UsersS')->where('mobile', $mobile)->find();
            switch ($smsType)
            {
                case 'reg':
                case 'um':
                    if($res) return $this->returnJson([], 1, '该手机号已经存在！');
                    break;
                case 'login':
                case 'forget':
                    if(!$res) return $this->returnJson([], 1, '该手机号不存在！');
                    break;
            }
        }
        $code = rand('100000', '999999');
        //判断是否已经发送
        if(cache($mobile.$smsType)){
            return $this->returnJson([], 1, '多长时间内不可以重复获取验证码！');
        }

        //发送验证短信
        SendSms::send($mobile, ['code'=>$code], $smsType);  //通过前端传递，$smsType
        return $this->returnJson();
    }

    /**
     * 用户登录
     */
    public function login(Request $request)
    {
        if($request->isPost()){
            //判断当前用户是否已登录
            if(self::$userInfo){
                $this->redirect('Index/index');
            }

            $params = input('post.');

            $scene = $params['scene'];

            //验证提交数据
            $validate=validate("Users");      //path: app\common\validate\Users
            if(!$validate->scene($scene)->check($params)){
                return $this->error($validate->getError());
            }

            //短信验证码登录
            if($scene == 'm_login'){
                //判断验证码
                if($params['code'] != cache($params['mobile'].'login')){
                    $this->error('验证码错误！');
                }
            }else{
                //密码登录
                $where['password'] = md5($params['password']);
            }

            $usersMdl = model('UsersS');
            $where['mobile'] = $params['mobile'];

            $userRes = $usersMdl->where($where)->find();
            if($userRes){
                //删除验证码缓存
                rmCache($params['mobile'].'login');
                $session_id = session_id();
                cookie('PHPSESSID', $session_id, 3600*24);
                //登录后，缓存session.
                session('user', $userRes->toArray());
                session('sid', $session_id);
            }else{
                $this->error('账户或者密码错误，请重登录！');
            }

            // $this->success('登录成功', url('Index/index'));
            $this->redirect('Index/index');
        }

    }

    /**
     * 用户退出
     */
    public function logout()
    {
        \think\Cookie::delete('PHPSESSID');
        session('user', null);
        $this->redirect('Index/index');
    }
    /**
     * 新用户注册
     */
    public function reg(Request $request){
        //判断是否为post提交
        if($request->isPost()){
            //获取数据
            $params = $request->except(['/'.$request->path(), 'file']);
            //验证提交数据
            $validate=validate("Users");      //path: app\common\validate\Users
            if(!$validate->scene('reg')->check($params)){
                return $this->error($validate->getError());
            }

            if(($params['code']??'') != cache($params['mobile'].'reg')){
                $this->error('验证码错误！');
            }

            //删除验证码缓存
            rmCache($params['mobile'].'reg');

            $usersMdl = model('UsersS');
            $where = [
                'mobile'=>$params['mobile']
            ];
            //创建用户
            Db::startTrans();
            try {
                $data = [
                    'mobile'=>$params['mobile'],
                    'level'=>1,
                    'password'=>md5($params['password']),
                    'name'=>$params['mobile'],
                    'c_time'=>time(),
                ];
                //推荐注册用户id
                $r_user_is = '';
                if($params['recommend_user_id']){
                    $r_id = explode('-', $params['recommend_user_id']);
                    $r_user_is = $usersMdl->where('id', $r_id['1'])->find();
                    $data['recommend_user_id'] = $r_id['1'];
                }

                $userId = $usersMdl->insertGetId($data);

                //插入推荐表数据
                if(isset($data['recommend_user_id']) && $data['recommend_user_id'] && $r_user_is){
                    //获取推荐用户的金额
                    $recommend_conf = get_A_conf('recommend','recommend');
                    $recommend = [
                        'user_id'=>$data['recommend_user_id'],
                        'new_id' =>$userId,
                        'user_time'=>time(),
                        'status'=>0,
                        'get_money'=>$recommend_conf['regUser'],
                        'is_grant'=>0
                    ];
                    model('Recommend')->save($recommend);
                }
                Db::commit();
            }catch (Exception $e){
                Db::rollback();
                if($params['recommend_user_id']){
                    mylog(['desc'=>'推荐注册新用户','msg'=>$e->getMessage(),'data'=>$data,'lastSql'=>model('Recommend')->getlastsql()],'user');
                    mylog(['desc'=>'注册新用户','msg'=>$e->getMessage(),'data'=>$data,'lastSql'=>model('UsersS')->getlastsql()],'user');
                }else{
                    mylog(['desc'=>'注册新用户','msg'=>$e->getMessage(),'data'=>$data,'lastSql'=>model('UsersS')->getlastsql()],'user');

                }
                return $this->error('注册失败，请刷新重试，或与管理员联系！');
            }
            if($userId){
                //查询用户，保存用户信息到session中
                $userRes = $usersMdl->where('id', $userId)->find();
                //登录后，缓存session.
                $session_id = session_id();
                cookie('PHPSESSID', $session_id, 3600*24);
                session('user', $userRes->toArray());
                session('sid', $session_id);
                $this->success('用户注册成功', url('Index/index'));
            }
        }
    }

    /**
     * 检查用户输入的手机号是否已经注册，如果注册，就提示，并跳转到登录页面
     */
    public function ajaxReg()
    {
        if(request()->isAjax()){
            if(UserCheck::is_reg(input('mobile'))){
                return $this->returnJson([],1, '该手机号已经注册过！');
            }
        }
    }

    /**
     * 忘记密码
     */
    public function forget()
    {
        //获取当前用户手机号
        if(request()->isPost()){
            //判断当前用户是否已登录
            if(self::$userInfo){
                $this->redirect('Index/index');
            }

            $params = request()->except(['/'.request()->path(), 'file']);

            //验证提交数据
            $validate=validate("Users");      //path: app\common\validate\Users
            if(!$validate->scene('forget')->check($params)){
                return $this->error($validate->getError());
            }

            //判断验证码
            if($params['code'] != cache($params['mobile'].'forget')){
                return $this->error('验证码错误，请重新输入');
            }

            //查询用户id
            $userInfo = model('UsersS')->where('mobile', $params['mobile'])->find();
            if(!$userInfo){
                return $this->error('手机对应用户不存在！');
            }
            //更新用户密码
            $data = [
                'id'=>$userInfo['id'],
                'password'=>md5($params['password']),
                'u_time'=>time()
            ];
            Db::startTrans();
            try{
                model('UsersS')->isUpdate(true)->save($data);
                Db::commit();
            }catch (Exception $e){
                Db::rollback();
                return $this->error('修改密码失败');
            }
            return $this->success('修改密码成功');
        }
        return $this->fetch();
    }

    
}
