<?php
namespace app\admin\model;

use think\Model;

class Menus extends Model{
    public $table = 'admin_menus';
    public $pk = 'id';
}