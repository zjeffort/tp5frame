<?php
namespace Pay\alipay\lib;

use think\Loader;
use Pay\alipay\kernel\buildermodel\AlipayTradePagePayContentBuilder;
//use Pay\alipay\kernel\service\AlipayTradeService;
Loader::import('Pay.alipay.kernel.service.AlipayTradeService');

class Base
{
    public $conf = [];
    public function __construct()
    {
        $payConf = [];
        $payConf = get_A_conf('payment', 'alipay');    //app_id,merchant_private_key,alipay_public_key
        $payConf['notify_url'] = 'http://'.$_SERVER['HTTP_HOST'].url('Pay/notify');   //支付回调
        $payConf['return_url'] = 'http://'.$_SERVER['HTTP_HOST'].url('Members/paySuccUpdateUser');   //支付跳转
        $payConf['charset'] = "UTF-8";
        $payConf['sign_type'] = "RSA2";
//        $payConf['gatewayUrl'] = "https://openapi.alipay.com/gateway.do";    //正式
        $payConf['gatewayUrl'] = "https://openapi.alipaydev.com/gateway.do";    //测试
        $this->conf = $payConf;
    }

    //构造参数
    public function payContentBuilder($data)
    {
        $payRequestBuilder = new AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($data['body']);
        $payRequestBuilder->setSubject($data['subject']);
        $payRequestBuilder->setTotalAmount($data['total_amount']);
        $payRequestBuilder->setOutTradeNo($data['out_trade_no']);
        $payRequestBuilder->setPassbackParams($data['passback_params']);        //公用回传参数
        return  $payRequestBuilder;
    }

    //支付请求
    public function tradeService($payRequestBuilder)
    {
        //获取配置
        $aop = new \AlipayTradeService($this->conf);
        //电脑网站支付请求
        $response = $aop->pagePay($payRequestBuilder,$this->conf['return_url'],$this->conf['notify_url']);

        return $response;
    }
}