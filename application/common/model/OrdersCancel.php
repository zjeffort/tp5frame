<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class OrdersCancel
 * 订单撤销
 * @package app\common\model
 */
class OrdersCancel extends Model{
    public $table = 'order_cancel';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    //获取器
    public function getImagesAttr($value)
    {
        return json_decode($value, true);
    }

}
