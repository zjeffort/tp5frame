<?php
namespace app\common\validate;

use think\Validate;

class Contract extends Validate{
    //规则
    protected $rule = [
        "id"        =>  "require|token",
        "__token__"   =>  "require",
        "role"   =>  "require",
        "money|合同金额"   =>  "require|float",
//        "desc|合同说明"   =>  "require",
        "images|合同文件"   =>  "require",
     ];

     protected $message =   [
         "id.token"      =>  "非法操作，不能重复提交",
     ];

     //场景
    protected $scene    =   [

    ];
}