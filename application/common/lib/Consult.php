<?php
namespace app\common\lib;

use think\Db;

/**
 * Class Consult
 * 后去地区名称
 * @package app\common\lib
 */
class Consult{
    /**
     * @param $p省 code
     * @param $c市 code
     * @param $a区 code
     * @return bool
     */
    static public function ConsultName($v=''){
        if(!$v){
            return;
        }
        $info = Db::table('region')->where('code', $v)->find();
        return $info['name'];
    }
}
