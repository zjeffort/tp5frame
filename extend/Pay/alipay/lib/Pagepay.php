<?php
namespace Pay\alipay\lib;

/**
 * Class pagepay
 * @package Pay\alipay\lib
 * PC页面跳转支付，跳转到支付宝页面，扫码支付
 */
class Pagepay extends Base
{
    public function pay($params)
    {
//        //获取支付参数
//        $payRequestBuilder = $this->payContentBuilder($params);

//        //配置参数
        $this->tradeService($this->payContentBuilder($params));
    }
}