<?php
namespace app\common\crontab;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Db;
/**
 * Class Order
 *  律师过期，定时任务
 * @package app\common\lib
 */
class Law extends Command
{
    protected function configure()
    {
        $this->setName('law')
            ->setDescription('定时计划：律师过期');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->zjdq();
        $this->qydq();
    }

    //证件到期
    private function zjdq()
    {
        $lawIds = Db::table('lawyer')->where('end_time','<',time())->where('status', 1)->column('id');

        //没有数据，返回
        if(count($lawIds) < 1){
            return ;
        }

        //获取律师id
//        $tmpLawId = [];
//        foreach ($laws as $v) {
//            $tmpLawId[] = $v['id'];
//        }

        //修改状态，2:证件到期
        Db::startTrans();
        try {
            Db::table('lawyer')->where('id', 'in', $lawIds)->update(
                ['status' => 2, 'u_time'=>time()]
            );
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            mylog(['desc'=>'crontab律师证件到期','msg'=>$e->getMessage(),'data'=>$lawIds,'lastSql'=>model('Lawyer')->getlastsql()], 'cron');
        }

    }

    //签约到期
    private function qydq()
    {
        $lawIds = Db::table('lawyer')->where('sign_end_time','<',time())->where('level', 1)->where('status', 1)->column('id');

        //没有数据，返回
        if(count($lawIds) < 1){
            return ;
        }

        //获取律师id
//        $tmpLawId = [];
//        foreach ($laws as $v) {
//            $tmpLawId[] = $v['id'];
//        }

        //修改状态，2:证件到期
        Db::startTrans();
        try {
            Db::table('lawyer')->where('id', 'in', $lawIds)->update(
                ['level' => 0, 'u_time'=>time()]
            );
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            mylog(['desc'=>'crontab律师签约到期','msg'=>$e->getMessage(),'data'=>$lawIds,'lastSql'=>model('Lawyer')->getlastsql()], 'cron');
        }
    }

}