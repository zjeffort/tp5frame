<?php
namespace app\common\model;

use think\Model;

/**
 * Class Recommend
 * 推荐
 * @package app\common\model
 */
class Recommend extends Model{
    public $table = 'recommend';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    public function getUserIdAttr($value, $data)
    {
        $info = db('users')->where('id', $value)->find();
        return $info;
    }

    public function getNewIdAttr($value, $data)
    {
        //1表示律师,0表示用户
        if($data['status'] == 1){
            $info = model('Lawyer')->where('id', $value)->find()->toArray();
        }else{
            $info = db('users')->where('id', $value)->find();
        }
        return $info;
    }

    public function getGetMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}
