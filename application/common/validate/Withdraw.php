<?php
namespace app\common\validate;

use think\Validate;

class Withdraw extends Validate{
    //规则
    protected $rule = [
        "card|提现账户"        =>  "require|token",
        "__token__"   =>  "require",
        "money|提现金额"   =>  "require|float|>:0.01",
        "phone|手机号"   =>  "require|regex:/^1[34578]{1}\d{9}$/",
        "code|验证码"   =>  "require",

     ];

     protected $message =   [
         "card.token"      =>  "非法操作"
     ];

     //场景
    protected $scene    =   [
    ];
}