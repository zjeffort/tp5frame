<?php
namespace app\testcase\controller;

use think\Controller;

class Base extends Controller{

    static public $userID = '';
    static public $userInfo = '';

    public function _initialize(){

        define('MODULE_NAME', request()->module());
        define('CONTROLLER_NAME', request()->controller());
        define('ACTION_NAME', request()->action());
        $box_is_pjax = $this->request->isPjax();
        $this->assign('box_is_pjax', $box_is_pjax);
    }

    //返回json
    protected  function returnJson($data=[], $err = 0, $msg = ''){
        $result = $this->returnArr($data,$err,$msg);
        return json($result);
    }

    //返回数组
    protected  function returnArr($data=[], $err = 0, $msg = ''){
        $result = [
            "err"   =>  $err,
            "msg"   =>  $this->getMsg($err, $msg),
            "data"  =>  $data,
        ];
        return $result;
    }

    public function getMsg($err,$msg){
        if($err){
            $msg = $msg ?? '失败';
        }else{
            $msg = '成功';
        }
        return $msg;
    }

    //获取用户id
//    public function getUserId(){
//        return session("user")['id'];
//    }
}
