<?php
namespace app\common\validate;

use think\Validate;

class Score extends Validate{
    //规则
    protected $rule = [
        "id|评价订单"        =>  "require|token",
        "__token__"   =>  "require",
        "desc|评价内容"   =>  "require",
        "score|评分"   =>  "require",
     ];

     protected $message =   [
         "filing.token"      =>  "非法操作，不能重复提交",
     ];

     //场景
    protected $scene    =   [

    ];
}