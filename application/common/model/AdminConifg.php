<?php
namespace app\common\model;

use think\Model;

/**
 * Class AdminConifg
 * 全局配置
 * @package app\common\model
 */
class AdminConifg extends Model{
    public $table = 'admin_config';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;
    static public $all_config_list = '';

    public function initialize()
    {
        parent::initialize();
        $config_name = input('config_name', '');
        self::$all_config_list = [
            $config_name =>config('AdminConfig.'.$config_name),
        ];
    }

    //获取器
    public function getConfigValAttr($value)
    {
        return json_decode($value, true);
    }

}
