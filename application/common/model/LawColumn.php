<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class LawColumn
 * @packageName 法律专栏Model
 */
class LawColumn extends Model{
    public $table = 'law_column';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;
}
