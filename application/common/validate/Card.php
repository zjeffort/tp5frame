<?php
namespace app\common\validate;

use think\Validate;

class Card extends Validate{
    //规则
    protected $rule = [
        "status|账户类型"        =>  "require",
//        "__token__"   =>  "require",
        "user_name|姓名"   =>  "require",
        "number|收款帐号"   =>  "require",
        "account_name|银行名称"   =>  "require|max:8",
        "open_account_name|开户行"   =>  "require",
        "phone|预留手机号"   =>  "require|regex:/^1[34578]{1}\d{9}$/",

     ];

     protected $message =   [
//         "status.token"      =>  "非法操作",
     ];

     //场景
    protected $scene    =   [
        'ali'=>[
            'user_name','number'
        ],
        'bank'=>[
            'account_name','open_account_name','phone','user_name','number'
        ],
    ];
}