<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class Rules
 * @packageName 关于我们->常见问题Model
 */
class Problem extends Model{
    public $table = 'admin_problem';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;
}
