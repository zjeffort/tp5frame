<?php
return [
    //七牛云配置
    'qiniu'=> [
        'remake'=>'七牛云配置', //用户数据库中描述，不会显示出来
        'AccessKey'=>'AccessKey',
        'SecretKey'=>'SecretKey',
        'bucket'=>'bucket(存放容器)',
        'domain'=>'domain(域名)',
        'videoSize'=>'video(视频大小Mb)',
        'audioSize'=>'audio(音频大小Mb)'
    ],
    //阿里云短信配置
    'alisms'=> [
        'remake'=>'阿里云短信配置',
        'accessKeyId'=>'accessKeyId',
        'accessSecret'=>'accessSecret',
    //        'regionId'=>'',
    //        'version'=>'',
        'signName'=>'签名名称',
        'reg'=>'注册模板号',
        'login'=>'登录模板号',
        'forget'=>'忘记密码模板号',
    ],
    //banner图配置
    'banner'=> [
        'remake'=>'banner图',
        'index'=>['首页', 'image'],
        'other'=>['其他页面', 'image'],
    ],
    //站点配置
    'site'=>[
        'name'=>'站点名称',
        'serviceTel'=>'客服电话',
        'reg' =>['用户注册协议', 'textarea'],
        'remake'=>'站点配置',
        'logo'=>['站点logo', 'image'],
        'footerLogo'=>['站点底部logo', 'image'],
        'icon'=>['icon图标', 'image'],
        'copyright'=>'版权所有',
        'record'=>'备案号',
        'description'=>'站点关键字',
        'keyword'=>'站点描述'
    ],
    //微信支付
    //Native支付：该模式适用于PC网站支付，生成支付二维码。
    'wxpay'=>[
        'remake'=>'微信Native支付',
        'KEY'=>'API密钥',
        'Appsecret'=>'Appsecret',
        'APPID'=>'APPID',
        'MCHID'=>'微信支付商户号',
    ],
    'alipay'=>[
        'remake'=>'阿里当面付(PC)',
        'app_id'=>'app_id',
        'merchant_private_key'=>'私钥',
        'alipay_public_key'=>'公钥',
    ],
    'wxchat'=>[
        'remake'=>'微信公众号',
        'app_id'=>'app_id',
        'appsecret'=>'appsecret',
        '模板消息id'=>'模板消息id',
    ],
];

