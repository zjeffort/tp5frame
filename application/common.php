<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

//自定义日志文件
function mylog($data, $fileName){

    if(is_null($data) || is_null($fileName)){
        return '';
    }

    $filePath = LOG_PATH . $fileName.'.log';

    if(!is_dir(LOG_PATH)){
        $mkdir_re = mkdir(LOG_PATH,0777,TRUE);
        if(!$mkdir_re){
            log('创建日志文件夹失败！');
        }
    }
    $time = date("Y-m-d H:i:s",time());

    $desc = [
        'DelResource'=>'删除失败资源文件',
        'sql'=>'执行sql失败',
        'pay'=>'支付日志',
        'cron'=>'定时任务',
        'user'=>'前端用户',
        'test'=>'测试日志'
    ];

    $re = file_put_contents($filePath, $desc[$fileName] ." : ". $time." \r\n 请求数据：".var_export($data,TRUE)."\r\n\r\n", FILE_APPEND);
}

//发起一个post
function curl_post($url='',$data=[]){
    $data = is_array($data) ? json_encode($data,JSON_UNESCAPED_UNICODE) : $data;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $info = curl_exec($ch);

    if(curl_errno($ch)){
        return [
            "err" => 1,
            "msg" => 'Errno'.curl_error($ch),
        ];
    }
    curl_close($ch);
    return [
        "err" => 0,
        "data"=> $info,
    ];
}

function getImageUrl($url=''){
    if( !empty($url) ){
        $url = substr($url,0,4)=="http" ? $url : "http://".$_SERVER["HTTP_HOST"].$url;
    }
    return $url;
}

function createQRcode($qrData)
{
    $PNG_TEMP_DIR = APP_PATH . '/../public/qrcode/';

    //检测并创建生成文件夹
    if (!file_exists($PNG_TEMP_DIR)) {
        mkdir($PNG_TEMP_DIR, 0755, true);
    }
    $filename = $PNG_TEMP_DIR.md5($qrData).'.png';

    if (file_exists($PNG_TEMP_DIR . basename($filename))) {
        return '/qrcode/'.basename($filename);
    }

    \PHPQRCode\QRcode::png($qrData, $filename, '', 6);

    return '/qrcode/'.basename($filename);
}

function ldump($data){
    error_log(print_r($data,1).PHP_EOL, 3, "F:/PHPTutorial/log/123.log");
//    error_log(print_r($data,1).PHP_EOL, 3, "/home/www/ldump.log");
}

/**
 * 获取律师id
 * @param $user_id 用户id
 * @return mixed
 */
function getLawId($user_id){
    return \app\common\lib\LawInfo::getLawId($user_id);
}

//删除缓存
function rmCache($name){
    cache($name, NULL);//删除缓存
}

function updateUserInfo($userID){
    $userRes = db('users')->where('id', $userID)->find();
    session('user', $userRes);
}
//function getLawId($user_id){
//    return \app\common\lib\LawInfo::getLawId($user_id);
//}
//
//function getLawId($user_id){
//    return \app\common\lib\LawInfo::getLawId($user_id);
//}
