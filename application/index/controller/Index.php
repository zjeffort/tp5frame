<?php
namespace app\index\controller;

use think\Db;
use think\Request;
use app\common\lib\Region;

/**
 * Class Index
 * 首页
 * @package app\index\controller
 */
class Index extends Base
{
    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        return $this->fetch();
    }

    //获取地址三级联动
    public function region(Request $request)
    {
        if($request->isPost()){
            $data = input('post.');
            if(isset($data['p']) && $data['p']){//市
                $res = Region::city($data['p']);
            }elseif(isset($data['c']) && $data['c']){//区
                $res = Region::area($data['c']);
            }else{
                return $this->returnJson('',1,'参数错误');
            }
            return $this->returnJson($res);
        }else{
            return $this->returnJson('',1,'参数错误');
        }
    }


}
