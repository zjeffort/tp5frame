<?php
namespace app\common\validate;

use think\Validate;

class Ensure extends Validate{
    //规则
    protected $rule = [
        "filing|是否立案"        =>  "require|token",
        "__token__"   =>  "require",
        "court_name|法院名称lll"   =>  "require",
        "guarantee_money|担保金额|必须是数字"   =>  "require|float",
        "tel|申请人联系电话|请输入正确的手机号"   =>  "require|regex:/^1[34578]{1}\d{9}$/",
        "name|申请人姓名"   =>  "require",
        "e_mail|申请人邮箱|邮箱"   =>  "require|email",
     ];

     protected $message =   [
         "filing.token"      =>  "非法操作，不能重复提交",
     ];

     //场景
    protected $scene    =   [

    ];
}