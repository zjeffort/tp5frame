<?php
namespace app\common\crontab;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Db;
/**
 * Class Order
 *  签约用户过期，定时任务
 * @package app\common\lib
 */
class User extends Command
{

    protected function configure()
    {
        $this->setName('user')
             ->setDescription('定时计划：签约用户过期');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->qydq();
    }

    //签约到期
    private function qydq()
    {
        $userIds = Db::table('users')->where('sign_end_time','<',time())->where('level', 2)->column('id');

        //没有数据，返回
        if(count($userIds) < 1){
            return ;
        }

        //获取律师id
//        $tmpUsersId = [];
//        foreach ($users as $v) {
//            $tmpUsersId[] = $v['id'];
//        }

        //修改状态，2:证件到期
        Db::startTrans();
        try {
            Db::table('UsersS')->where('id', 'in', $userIds)->update(
                ['level' => 1, 'u_time'=>time()]
            );
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            mylog(['desc'=>'用户签约到期','msg'=>$e->getMessage(),'data'=>$userIds,'lastSql'=>model('UsersS')->getlastsql()], 'cron');
        }
    }

}