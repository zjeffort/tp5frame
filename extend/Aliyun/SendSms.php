<?php

namespace Aliyun;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class SendSms {

    private $accessKeyId    ='';    // key
    private $accessSecret   ='';    // Secret
    private $regionId       ='';    // 请替换为自己的 Region ID
    private $product        ='';    // 指定产品
    private $version        ='';    // 指定版本
    private $action         ='';    // 指定接口
    private $phone          ='';    // 接收手机号
    private $code           ='';    // 验证码
    private $smsType        ='';    // 消息类型
    private $templateCode   ='';    //

    public function __construct($phone, $code, $smsType, $templateCode)
    {
//        $this->accessKeyId  = $accessKeyId;
//        $this->accessSecret = $accessSecret;
//        $this->regionId     = $regionId;
//        $this->product      = $product;
//        $this->version      = $version;
//        $this->action       = $action;
//        $this->phone        = $phone;
//        $this->code         = $code;
//        $this->smsType      = $smsType;
//        return $this->send($this);
    }

    /**
     * @param $phone        //手机号
     * @param array $data   //验证码，['code'=>123456]
     * @param $smsType      //发送模板类型
     * @param string $sendType // 指定接口
     * @return bool
     * @throws ClientException
     */
    static public function send($phone, $data=[], $smsType, $sendType='sendSms')
    {
//ldump($phone.'-'.$smsType.'-'.$data['code']);
//        cache($phone.$smsType, $data['code'],60); //todo 要区分不同短信类型
//        return true;
        //获取短信配置
        $confInfo = get_A_conf('alisms','alisms');

        if(!$confInfo || !array_key_exists($smsType, $confInfo)){
            return false;
        }
        $sendData = [
            'PhoneNumbers'  => $phone,                                  //手机号
            'TemplateCode'  => $confInfo[$smsType],                     //SMS_163625254
            'SignName'      => $confInfo['signName'],                   //网慧天下demo
            'TemplateParam' => json_encode($data),                      //示例['code'=>123456]
        ];
        AlibabaCloud::accessKeyClient($confInfo['accessKeyId'], $confInfo['accessSecret'])
        ->regionId('cn-hangzhou') // 请替换为自己的 Region ID
        ->asGlobalClient();
        try {
            $result = AlibabaCloud::rpcRequest()    // 指定接口风格
                ->product('Dysmsapi')           // 指定产品
                // ->scheme('https')                // https | http
                ->version('2017-05-25')           // 指定版本
                ->action($sendType)             // 指定接口
                ->method('POST')
                ->options([
                    'query' => $sendData,
                ])
                ->request();
            //缓存短信，1分钟
//            ldump($result->toArray());
//            ldump($result->toArray());
            if($data['code']??''){
                cache($phone.$smsType, $data['code']??'',60); //todo 要区分不同短信类型
            }
//            print_r($result->toArray());
        } catch (ClientException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        }
    }
}