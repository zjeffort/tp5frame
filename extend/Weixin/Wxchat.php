<?php
namespace Weixin;

use think\Exception;
use think\Session;

class Wxchat {

    public $AppID = '';
    public $AppSecret = '';
    public $token = '';
    public $serverUrl = '';
    public $openid = '';
    public $wxchatConf = '';

    public function __construct()
    {
        $this->wxchatConf = get_A_conf('wxchat', 'wxchat');
        $this->AppID = $this->wxchatConf['app_id'];
        $this->AppSecret = $this->wxchatConf['appsecret'];
        $this->serverUrl = request()->domain();
        $this->token = $this->getWxAccessToken();
    }

    public function index(){
#       dump($this->token);
//       dump($this->geiBaseInfo());     //获取用户openid
//        dump($this->getUserInfo());     //获取用户基本信息
    }

    //发送模板消息
    public function sendTemplateMsg($openid ,$url , $msg){
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$this->token;
        $data = [
            "touser"      => $openid,                        //要发送的openid
            "template_id" => $this->wxchatConf['yjxy_id'], //模板id
            "url"         => $url,
            "data"  =>  [
                'yjxy'=>array('value'=>$msg, 'color'=>'#173177'),
//                "first"  =>  [
//                    "value"  =>  $value,
//                    "color"  =>  "#173177",
//                ],
//                "keyword1"  =>  [
//                    "value"  =>  $user["u_time"],
//                    "color"  =>  "#173177",
//                ],
//                "keyword2"  =>  [
//                    "value"  =>  $jieguo,
//                    "color"  =>  "#173177",
//                ],
//                "remark"  =>  [
//                    "value"  =>  $remark,
//                    "color"  =>  "#173177",
//                ],
            ],
        ];
        curl_post($url, $data); //todo
    }


    //接收事件推送并回复，testCase。
    public function responseMsg(){
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$this->token;
        $data = [
            "touser"      => $user["wechat_openid"],                        //要发送的openid
            "template_id" => "uWLZHb6pM8HtU31qftEmgFKaAQX3Hm-MpcOyBzp2fX8", //模板id
            "url"         => "http://www.baidu.com",
            "data"  =>  [
                'test'=>array('value'=>'xxxxxaaaaa', 'color'=>'#173177'),
//                "first"  =>  [
//                    "value"  =>  $value,
//                    "color"  =>  "#173177",
//                ],
//                "keyword1"  =>  [
//                    "value"  =>  $user["u_time"],
//                    "color"  =>  "#173177",
//                ],
//                "keyword2"  =>  [
//                    "value"  =>  $jieguo,
//                    "color"  =>  "#173177",
//                ],
//                "remark"  =>  [
//                    "value"  =>  $remark,
//                    "color"  =>  "#173177",
//                ],
            ],
        ];
    }

    public function messageSend()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$this->token;
        $data = [
            "touser"=>"o-XUg1BSR_Y5NqZxMX2AffMI6K-E",
            "msgtype"=>"text",
            "text"=>[
                "content"=>"sdlkfjslfjsdf夺123"
            ]
        ];

    }

    //https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN
    //微信公众号菜单自定义
    public function menus()
    {
        try{

        }catch (Exception $e){

        }
    }

    private function http_curl($url){
        //1.初始化curl
        $ch = curl_init();
        // $url = $url;
        //2.设置curl的参数
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//如果成功只将结果返回，不自动输出任何内容。如果失败返回FALSE
        //3.采集
        $output = curl_exec($ch);
        //4.关闭
        curl_close($ch);
        // if(curl_errno($ch)){
        //     var_dump( curl_error($ch) );
        // }
        $arr = json_decode($output, true);
        return $arr;

    }

    //获取token
    public function getWxAccessToken(){
        if(Session::get('wx_token_time') > time() && Session::get('wx_token')){
            $token = Session::get('wx_token');
        }else{
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$this->AppID."&secret=".$this->AppSecret;
            $token = $this->http_curl($url);
            $token = $token['access_token'];
            Session::set('wx_token',$token);
            Session::set('wx_token_time',time()+7000);
        }
        return $token;
    }

    //签名随机字串16位，按微信文档上来的
    public function nonceStr($num = 16){
        $arr = array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            '0','1','2','3','4','5','6','7','8','9'
        );
        $tmpstr = '';
        $tmparr = array();
        $strmax = count($arr);
        for($i=1;$i<=$num;$i++){
            $rand = rand(0,$strmax-1);
            $tmpstr .= $arr[$rand];
        }
        return $tmpstr;
    }

    //获取用户的OPENID
    public function geiBaseInfo(){
        //通过code获得openid
        $code = input('code', '');
        if ( !$code ){
            //触发微信返回code码
//            $baseUrl = urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$_SERVER['QUERY_STRING']);
            $baseUrl = urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
            $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$this->AppID."&redirect_uri=".$baseUrl."&response_type=code&Scope=snsapi_base&state=STATE#wechat_redirect";
            Header("Location: $url");
            exit();
        } else {
            //获取code码，以获取openid
            $openid = $this->getUserOpenid($code);
            return $openid;
        }
    }

    public function getUserOpenid($code){
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$this->AppID ."&secret=".$this->AppSecret."&code=".$code."&grant_type=authorization_code";
        $res = $this->http_curl($url);
//        $this->openid = $res['openid'];
        if($res['openid']??''){
            return $res['openid'];
        }else{
            mylog(['desc'=>'微信获取openid回调错误','data'=>$res], 'test');
        }
    }

    //获取用户基本信息
    public function getUserInfo()
    {
        $openid = $this->geiBaseInfo();
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$this->token."&openid=".$openid."&lang=zh_CN";
        $res = $this->http_curl($url);
//        var_dump($res);
        return $res;
    }
}