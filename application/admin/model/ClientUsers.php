<?php
namespace app\admin\model;

use think\Model;

class ClientUsers extends Model{
    public $table = 'users';
    public $pk = 'id';

    //获取器
    public function getLevelAttr($value)
    {
        $status = ['1'=>'普通用户','2'=>'签约用户'];
        return $status[$value];
    }

    public function getMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}