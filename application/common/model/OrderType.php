<?php
namespace app\common\model;

use think\Model;

/**
 * Class OrderType
 * 订单类型
 * @package app\common\model
 */
class OrderType extends Model{
    public $table = 'order_type';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

//    public function validateReg($data){
//        var_dump($this->find());
//    }

    public function getPriceAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}
