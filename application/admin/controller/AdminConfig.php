<?php
namespace app\admin\controller;

use think\Db;
use think\Exception;
use think\Request;

class AdminConfig extends Base{

    public $db = '';
    public $action = '';    //当前请求方法

    public function _initialize()
    {
        parent::_initialize();
        $this->db       = model('AdminConifg');
        $this->action   = request()->action();
    }

    public function _empty(Request $request)
    {
        if($request->isPost()){    //保存配置信息
            return $this->store($request);
        }

        $config_name = input("config_name")?:'site';    //默认显示站点配置页

        $where = [
            'pre'=>$this->action,
            'config_name'=>$config_name
        ];
        $info = $this->db->where($where)->find();

        $this->assign(compact("info"));
        $this->assign('adminconfigInfo', $info['config_val']);
        $this->assign('c_name', $config_name);
        return $this->fetch($this->action);
    }

    //保存配置信息
    private function store(Request $request){
        $params = $request->except(['/'.$request->path(), 'file']);

        if($request->isPost() && $config_name = input("post.config_name") ){

        }else{
            return $this->ajaxReturn(1,"操作失败，请刷新重试");
        }

        //把页面中需要的input，k:v保留
        $tmp = $params;
        unset($tmp['id']);
        unset($tmp['remake']);
        unset($tmp['config_name']);

        Db::startTrans();
        try {
            $data = [
                'id' => $params['id'],
                'pre' => $this->action,
                'config_name' => $config_name,
                'config_val' => json_encode($tmp),      //配置项保存的内容
                'remake' => $params['remake'],
            ];
            if($params["id"]){
                $data['u_time'] = time();
                $this->db->update($data);
            }else{
                $data['c_time'] = time();
                unset($data["id"]);
                $this->db->insertGetId($data);
            }
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            return $this->ajaxReturn(1,"操作失败，请刷新重试");
        }
        return $this->ajaxReturn(0,"", url(CONTROLLER_NAME."/".$this->action, ['config_name'=>$config_name]));
    }

}
