<?php
namespace app\common\model;

use think\Model;
use think\Db;
use app\common\lib\Consult;

/**
 * Class Ensure
 * 保函申请
 * @package app\common\model
 */
class Ensure extends Model{
    public $table = 'ensure';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    public function getStatusAttr($value)
    {
        $status = ['1'=>'待处理','2'=>'处理中','3'=>'成功','4'=>'失败'];
        return $status[$value];
    }
    public function getFilingAttr($value)
    {
        $status = ['1'=>'否','2'=>'是'];
        return $status[$value];
    }

    public function getGuaranteeMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}
