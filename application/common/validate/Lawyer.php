<?php
namespace app\common\validate;

use think\Validate;

class Lawyer extends Validate{
    //规则
    protected $rule = [
        "name|姓名"        =>  "require|token",
        "__token__"   =>  "require",
        "c_image|律师背景图"   =>  "require",
        "image|头像"   =>  "require",
        "lawyer_license_image|律师执业证"   =>  "require",
        "year_check_image|年检页图片"   =>  "require",
        "office|所在事务所"   =>  "require",
        "end_time|执业证到期期限"   =>  "require",
        "speciality|专长"   =>  "require",
        "consult_p|省"=>"require",
        "consult_c|市"=>"require",
        "consult_a|区"=>"require",
     ];

     protected $message =   [
         "name.token"      =>  "非法操作，不能重复提交",
     ];

     //场景
    protected $scene    =   [

    ];
}