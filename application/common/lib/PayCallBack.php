<?php
namespace app\common\lib;

use think\Loader;
/**
 * Class PayCallBack
 * 支付回调
 * @package app\common\lib
 */
class PayCallBack{
    // 初始化签权对象
    public function __construct(){}

    //回调方法入口
    public static function notify($params)
    {
        //判断支付宝or微信回调
        if(isset($params['trade_status']) && $params['trade_status'] == 'TRADE_SUCCESS'){   //支付宝
            //获取配置，验证数据
            $base = new \Pay\alipay\lib\Base();
            Loader::import('Pay.alipay.kernel.service.AlipayTradeService');
            $alipaySevice = new \AlipayTradeService($base->conf);
            $result = $alipaySevice->check($params);
            if($result){
                return true;
            }else{
                mylog(['desc'=>'支付宝回调验证数据失败','data'=>$params], 'pay');
                return false;
            }
        }
        elseif  //微信回调
        (isset($params['result_code']) && isset($params['return_code']) && $params['result_code'] == 'SUCCESS' && $params['return_code'] == 'SUCCESS' && isset($params['transaction_id'])){
            //todo 可以添加数据验证！！！
            return true;
        }else{
            //记录日志！！
            mylog(['desc'=>'微信回调验证数据失败','data'=>$params], 'pay');
            return false;
        }
    }
}
