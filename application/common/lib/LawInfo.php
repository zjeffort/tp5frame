<?php
namespace app\common\lib;

/**
 * Class LawInfo
 * 获取律师信息，律师id
 * @package app\common\lib
 */
class LawInfo{

    static public function getLawInfo($id)
    {
        $params = [
            "id"    =>  $id,
        ];
        $info = db("lawyer")->where($params)->find();
        return $info;
    }

    static public function getLawMdlInfo($id)
    {
        $params = [
            "id"    =>  $id,
        ];
        $info = model("lawyer")->where($params)->find();
        return $info;
    }

    static public function getLawId($user_id)
    {
        $params = [
            "user_id"    =>  $user_id,
        ];
        $info = db("lawyer")->where($params)->find();
        return ($info ? $info['id'] : '');
    }

    static public function isLaw($user_id)
    {
        $params = [
            "user_id"    =>  $user_id,
            'status'     => 1
        ];
        $info = db("lawyer")->where($params)->find();
        return ($info ? $info['id'] : '');
    }

    static public function isLawVip($id)
    {
        $params = [
            "id"    =>  $id,
            'status'  => 1,
            'level' =>1
        ];
        $info = db("lawyer")->where($params)->find();
        return ($info ? $info['id'] : '');
    }
}
