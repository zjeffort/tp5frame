<?php
namespace app\common\model;

use think\Model;

/**
 * Class SignLaw
 * 用户的签约律师
 * @package app\common\model
 */
class SignLaw extends Model{
    public $table = 'sign_law';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

    public function getUserIdAttr($value)
    {
        $userInfo = model('UsersS')->where('id', $value)->find();
        return $userInfo;
    }
}
