<?php
namespace app\common\lib;

use think\Db;

/**
 * Class Order
 * 订单搜索
 * @package app\common\lib
 */
class Order{
    //省
    static public function province(){
        return Db::table('region')->where('pid','0')->select();
    }

    //搜索公用
    static function searchOrder($order_type_val = '', $searchOrder = false)
    {
        $returnData = [];
        //订单类型
        $orderType = model("OrderType")
            ->order('id asc, pid desc')->select();
        foreach($orderType as &$v){
            $v = $v->toArray();
        }

        $orderType = toTree($orderType);
        if($order_type_val){
            foreach ($orderType as $val){
                if($val['id'] == $order_type_val){
                    $orderType = $val;
                    break;
                }
            }
        }

        $returnData['orderType'] =  $orderType;
        if($searchOrder) return $returnData;
        //省
        $province = Region::province();
        $returnData['province'] =  $province;
        //市
        $city = Region::city(1);
        unset($city[0]);
        $returnData['city'] =  $city;

        return $returnData;
    }

    static function toSearch($params){
        if($params['type_id'] || $params['consult_p'] || $params['consult_c'] || $params['status'] || $params['user_id']){
            $where = [];
            if($params['type_id']){
                //返回该分类下的所有分类id
                $typeTreeId = self::typeTree($params['type_id']);
                $where['type_id'] = ['in',$typeTreeId.$params['type_id']];
            }
            if($params['consult_p']){
                $where['consult_p'] = $params['consult_p'];
                if($params['consult_c']){
                    $where['consult_p'] = $params['consult_p'];
                }
            }

            if($params['status']){
                $where['status'] = $params['status'];
            }

//            if($params['is_law_order']){
//                $where['is_law_order'] = $params['is_law_order'];
//            }

            if($params['user_id']){
                $userInfo = db('users')->where('mobile', $params['user_id'])->find();
                $where['user_id'] = $userInfo['id'];
            }
            return $where;
        }else{
            return [];
        }
    }

    static public function typeTree($pid)
    {
        static $typeID = '';
        $p = Db::table('order_type')->where('pid', $pid)->select();
        foreach ($p as $k=>$v){
            unset($p[$k]);
            $typeID .= $v['id'].',';
            self::typeTree($v['id']);
        }
        return $typeID;
    }

    //订单发布完成后相关操作
    public static function orderFinishHandle($params)
    {
        //签约用户发单
        /**
         * 1.1获取签自己的约律师openid
         * 1.2给自己的签约律师发送微信通知
         */
        //获取自己的签约律师openid
        $infos = model('SignLaw')->alias('s')
            ->join('lawyer l', 'l.id = s.law_id')
            ->join('users u', 'u.id = l.user_id')
            ->where('s.user_id', $params['user_id'])
            ->where('is_move=0 or is_move is null')
            ->field('u.wechat_openid,u.mobile')
//            ->field('u.wechat_openid')
            ->select();
        if(count($infos) < 1){
            return ;
        }

        $Wxchat = new \Weixin\Wxchat();

        $url = url('Members/signUser');
        $url = url('Orders/orderDetails',['id'=>$params['order_id']]);
        $msg = "您有一条签约用户的新订单，请注意查看。";
        foreach ($infos as $v) {
            if($v['wechat_openid']){
                $Wxchat->sendTemplateMsg($v['wechat_openid'] ,$url , $msg);
            }
        }

        //如果是一键响应，还要给律师发送短信
        if(isset($params['order_type']) && $params['order_type'] == 11){
            //发送一键响应短信
            //获取我签约律师手机号
            foreach ($infos as $v) {
                if($v['mobile']){
                    \Aliyun\SendSms::send($orderInfo['user_id']['mobile'],[], 'yjxy');
                }
            }
        }
    }
}
