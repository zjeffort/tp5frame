<?php
namespace app\common\lib;

/**
 * Class DelResource
 * 删除上传资源
 * @package app\common\lib
 */
class DelResource{
    /**
     * @param $data 删除的数据 string
     * @param $type 目前为保留字段。删除文件类型 image , video, audio
     */
    static public function del($data, $type = '')
    {
        if(is_array($data)){  //删除图片必须上数组
            //判断图片是否存在
            foreach ($data as $v){
                if(is_file(ROOT_PATH.'public'.$v) && unlink(ROOT_PATH.'public'.$v)){
                }else{
                    mylog($v,'DelResource');
                }
            }
        }else{
            //删除视频,音频
            $qn = new \app\common\lib\Qiniu();
            $res = $qn->delFile($data);
            if ($res) {
                mylog($data,'DelResource');
            }
        }
    }

    /**
     * @param $data 删除所有的数据 string
     * @param $type 目前为保留字段。删除文件类型 image , video, audio
     */
    static public function delAll($data)
    {
        if( !empty($data['images']) ){
            self::del( json_decode($data['images']) );
        }
        if($data['video']){
            self::del( $data['video'] );
        }
        if($data['audio']){
            self::del( $data['audio'] );
        }
    }
}
