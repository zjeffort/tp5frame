<?php
namespace Pay;

use think\Exception;
use think\Session;
use Pay\wx\lib\WxPayNotify;
use Pay\wx\example\WxPayConfig;

class PayCenter extends WxPayNotify
{
    public function __construct()
    {
        $this->Handle(new WxPayConfig(), false);
    }

    //回调函数
    public function NotifyProcess($objData, $config, &$msg)
    {
        $data = $objData->GetValues();

        //进行参数校验
        if(!array_key_exists("return_code", $data)
            ||(array_key_exists("return_code", $data) && $data['return_code'] != "SUCCESS")) {
            //TODO失败,不是支付成功的通知
            //如果有需要可以做失败时候的一些清理处理，并且做一些监控
            $msg = "异常异常";
            return false;
        }
        if(!array_key_exists("transaction_id", $data)){
            $msg = "输入参数不正确";
            return false;
        }

        //查询订单，判断订单真实性
//        if(!$this->Queryorder($data["transaction_id"])){
//            $msg = "订单查询失败";
//            return false;
//        }
        return true;
    }

}