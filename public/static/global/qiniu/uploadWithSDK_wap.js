//验证MP4,mp3
function fileCheck(file,size,type){
    var fileName = file.name;
    var extStart = fileName.lastIndexOf(".");
    var ext = fileName.substring(extStart,fileName.length).toUpperCase();    //toUpperCase  :  方法用于把字符串转换为大写
    // 判断格式
    var type=type;
    if (type=="video") {
        if(ext == '.MP4'){
            if(file.size > (size * (1024*1024)) ){
                alert(fileName + '文件不能大于' + size  +'MB');
                return false;
            }else{
                return true;
            }
        }else{
            alert(fileName + '格式不正确');
            return false;
        }
    } else {
        if(type=="audio"){
            if(ext == '.MP3'){
                if(file.size > (size * (1024*1024)) ){
                    alert(fileName + '文件不能大于' + size  +'MB');
                    return false;
                }else{
                    return true;
                }
            }else{
                alert(fileName + '格式不正确');
                return false;
            }
        } 
        return false;
    }
}
//上传
function uploadWithSDK(file, token, putExtra, config, domain, userId,type) {
    var finishedAttr = [];
    var compareChunks = [];
    var observable;
    var type=type;
    if (file) {
        // var key = file.name;
        var key = userId + "/" + md5(file.name.split(".")[0] + Date.parse(new Date())) + '.' + file.name.split(".")[1];
        //todo 获取进度条节点
        //todo 需要加，在页面中显示上传中
        var error = function(err) {
            console.log(err);
            alert('上传出错');
            return ;
        };
        var complete = function(res) {
            // alert("上传成功");
            var url = domain + '/' + key;
            //查看资源
            // $('.btn-success').attr('href', url);        //todo 以实际class为准
            //input值
            // $('#mp4').val(key);                         //todo 以实际id为准

            // layer.alert('上传成功！');
            alert('上传成功！')
            //todo 在页面中显示上传完成
            // left: 185px;
            // $('.btn-success').removeClass('hidden');
            // $('.btn-danger').removeClass('hidden');
            // $('#selectFile').css('left','185px');
            $("input[name="+type+"]").val(res.key);
            return ;
        };
        
        var next = function(response) {
            //进度条
            var total = response.total;
            var jindu=parseInt(total.percent);
            //进度条加载
            $('.'+type+'-progress .layui-progress-bar').css({"width":jindu+'%'});
            $('.'+type+'-progress .layui-progress-text').html(jindu+'%');
            
        };
        var subObject = {
            next: next,
            error: error,
            complete: complete
        };

        // 调用sdk上传接口获得相应的observable，控制上传和暂停
        observable = qiniu.upload(file, key, token, putExtra, config);
        //上传
        observable.subscribe(subObject);
    }
}