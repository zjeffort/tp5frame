<?php
namespace app\admin\controller;

use think\Db;
use think\Request;
use app\common\lib\SeeUserInfo;

class ClientUser extends Base
{
    public function _initialize()
    {
        parent::_initialize();
    }

    public function index(){
        $where = [];
        if(input('mobile','')){
            $where["mobile"]  =   input('mobile');
        }
        if(input('level','')){
            $where["level"]  =   input('level');
        }
//        if($search = input("get.search")){
//            $where["name|mobile"]  =   ["like", "%".$search."%"];
//        }
        $fieldsArr  =   [
            "id","avatar","name","mobile","level"
        ];
        $users = model("ClientUsers")
            ->where($where)
            ->field($fieldsArr)
            ->order("id","desc")
            ->paginate(15,false,page_param());
        $this->assign("data",$users);
        $placeHolderSearch = "姓名或手机号";
        $this->assign("placeHolderSearch",$placeHolderSearch);
        return $this->fetch();
    }

    public function edit(Request $request){
        if($request->isPost()){
            return $this->store($request);
        }
        $id =   input("route.id",0);
        $where  =   [
            "id"    =>  $id,
        ];
        $user   =   model("ClientUsers")
            ->where($where)
            ->find();
        $this->assign("info",$user);
        return $this->fetch();
    }

    private function store(Request $request){
        $params =   $request->only(["id"]);
        model("ClientUsers")->isUpdate(true)->save($params);
        $this->send($params["id"]);
        //发送微信模板通知
        return $this->ajaxReturn(0,'',url('ClientUser/index'));
    }

    //发送模板消息
    public function send($id=0) {
        $user = model("ClientUsers")->where(["id"=>$id])->find();
        if(!in_array($user["status"], [1,4])){
            return ["err" => 1, "msg"=>"不需要发送微信模板",];
        }
        $accessToken = (new Wechat())->getAccessToken($user["id"]);
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=$accessToken";  
        switch($user["status"]){
            case 1:
                $value = "您好,您的账号申请审核已通过";
                $jieguo= "审核通过";
                $remark= "感谢您的使用";
                break;
            case 4:
                $value = "您好,您的账号申请审核未通过";
                $jieguo= "审核未通过";
                $remark= "请您修改您的注册信息";
                break;
            case 2:
            case 3:
            case 5:
            default:
                break;
        }
        $data = [
            "touser"      => $user["wechat_openid"],
            "template_id" => "pvjhCHN5-sBETPOX5J3clsTpZRXTObNaeEbHUpN-Gec",
            "url"         => "",
            "data"  =>  [
                "first"  =>  [
                    "value"  =>  $value,
                    "color"  =>  "#173177",
                ],
                "keyword1"  =>  [
                    "value"  =>  $user["u_time"],
                    "color"  =>  "#173177",
                ],
                "keyword2"  =>  [
                    "value"  =>  $jieguo,
                    "color"  =>  "#173177",
                ],
                "remark"  =>  [
                    "value"  =>  $remark,
                    "color"  =>  "#173177",
                ],
            ],
        ];
        $result = curl_post($url, $data);  
        return $result;
    }

    //删除用户
    public function delete(Request $request){
        $params =   [
            "id"    =>  input("post.id/d",0),
        ];
        model("ClientUsers")->where($params)->delete();
        return $this->ajaxReturn();
    }

    //用户详情
    public function see_info()
    {
        $params =   input();
        if(!$params['id']){
            return $this->ajaxReturn(1,"请求失败！未获取到用户Id");
        }
        $id = $params['id'];

        //tag识别
        $act = $params['act'] ?? 'own';
        $this->assign("pageInfo",$act);

        //传递查看当前用户的id
        $res['id'] = $id;
        $this->assign("info",$res);

        //获取该用户对应信息
        $res = SeeUserInfo::$act($id);
        $this->assign($act,$res);

        return $this->fetch();
    }

    //查看律师详情  todo 没用到
    public function see()
    {
        $info = \app\common\lib\LawInfo::getLawInfo(input("id",0));
        $this->assign(compact("info"));
        return $this->fetch('lawyer/see');
    }

    //同意移除律师
    public function removeLaw()
    {
        if(request()->isPost()){
            Db::startTrans();
            try {
                $is_move = null;
                if(input('is_move')){
                    $is_move = 1;
                }
                Db::table('sign_law')->where('id',input('id',''))->update(['is_move' => $is_move, 'move_time'=>time()]);
                Db::commit();
            }catch (Exception $e){
                Db::rollback();
                return $this->ajaxReturn(1,"操作失败，请刷新重试");
            }
            return $this->ajaxReturn();
        }
        $signLaw = model('SignLaw')->alias('s')
            ->join('lawyer l', 's.law_id = l.id', 'left')
            ->join('users u', 'l.user_id = u.id', 'left')
            ->where('is_move',0)
            ->field(['s.*,l.name as l_name,u.mobile as u_mobile'])
            ->paginate();
        $this->assign(compact('signLaw'));
        return $this->fetch();
    }

    //导出excel
    public function userExport(){
        $where = [];
        if(input('mobile','')){
            $where["mobile"]  =   input('mobile');
        }
        if(input('level','')){
            $where["level"]  =   input('level');
        }
        //搜索出来的结果
        $users = db('users')->where($where)->field(['id, name, mobile, level, money, c_time'])->select();
        foreach ($users as &$v){
            $v['c_time'] = date('Y-m-d H:i:s', $v['c_time']);
            $v['level'] = $v['level'] == 1 ? '普通用户' : '签约用户';
        }
        //字段名称
        $keys = ['用户id','用户名','手机号','签约状态','余额','用户创建时间'];

        new \app\common\lib\Export($keys,$users,'用户表');
        exit();
    }

}
