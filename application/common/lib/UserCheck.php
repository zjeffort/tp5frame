<?php
namespace app\common\lib;

/**
 * Class UserCheck
 * 检查用户是否注册
 * @package app\common\lib
 */
class UserCheck{
    //判断用户是否登录,返回true表示状态为：登录
//    static public function is_login($mobile, $agent='web'){
//        //set         session('userLogin.web.1234',true);
//        if(session("userLogin.{$agent}.{$mobile}")) return true;
//    }

    //判断用户是否已注册
    static public function is_reg($mobile, $agent='web'){
        $usersMdl = model('UsersS');
        $userRes = $usersMdl->where('mobile', $mobile)->find();
        if($userRes){
            return true;
        }
        return false;
    }

    static public function isVip($user_id){
        $usersMdl = model('UsersS');
        $userRes = $usersMdl->where('id', $user_id)->where('level',2)->find();
        if($userRes){
            return true;
        }
        return false;
    }
}
