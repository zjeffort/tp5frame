<?php
namespace app\common\validate;

use think\Validate;

class Users extends Validate{
    //规则
    protected $rule = [
        "mobile"        =>  "require|regex:/^1[34578]{1}\d{9}$/",
//        "phonecheck"        =>  "regex:/^1[34578]{1}\d{9}$/",
//        "__token__"   =>  "require",
        "code|验证码"  =>  "require",
        "password|密码"  =>  "require|length:6,15|alphaDash",
        "password_original|密码" =>  "require|length:6,15|alphaDash|confirm:password",
     ];

     protected $message =   [
         "mobile.require"    =>  "手机号码必填,不能为空",
         "mobile.regex"      =>  "手机号码格式不正确",
//         "mobile.token"      =>  "非法操作，不能重复提交",
//         "password.require" =>  "密码填写,不能为空",
//         "password_original.confirm"    =>  "2次输入密码不一致，请重新输入",
     ];

     //场景
    protected $scene    =   [
        "reg"  =>  [
            "mobile","code","password"
        ],
        "m_login" => [   //手机验证码登录
            "mobile","code"
        ],
        "u_login" => [   //手机密码登录
            "mobile","password",
        ],
        "forget" => [
            "mobile","code","password","password_original"
        ]

    ];
}