<?php
namespace Pay\wxpay\example;

use Pay\wxpay\lib\WxPayData\WxPayUnifiedOrder;

class WxPayBase
{
    static public $conf = [];       //微信配置
    static public $params = [];       //微信请求参数

    public function __construct()
    {
        self::$conf = new WxPayConfig();                //获取微信配置
        self::$params = new WxPayUnifiedOrder();        //参数类
    }

}
