<?php
namespace app\common\model;

use think\Model;

//前台注册用户
class UsersS extends Model{
    public $table = 'users';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

//    public function validateReg($data){
//        var_dump($this->find());
//    }

    public function getMoneyAttr($value)
    {
        return sprintf("%.2f", $value);
    }

}
