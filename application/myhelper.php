<?php

if (!function_exists('get_A_conf')) {
    /**
     * 获取表admin_config数据
     * @param $pre  配置项前缀
     * @param $name 配置项名称
     */
    function get_A_conf($pre, $name)
    {
        $where = [
            'pre'=>$pre,
            'config_name'=>$name
        ];
        $res = model('AdminConifg')->where($where)->find();
        return $res['config_val'] ?? '';
    }
}

