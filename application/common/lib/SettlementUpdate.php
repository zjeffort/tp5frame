<?php
namespace app\common\lib;

use Aliyun\SendSms;
use think\Db;
/**
 * Class SettlementUpdate
 * 撤单,提现，消费，成功后的操作
 * @package app\common\lib
 */
class SettlementUpdate
{
    public function __construct(){}

    //订单撤销
    public static function cencel($order_id)
    {
        //获取订单信息
        $orderInfo = model('Orders')->where('id', $order_id)->where('cencel_money', null)->where('cencel_time', null)->find();
        if(!$orderInfo){
            return ;
        }
        //需要退还金额
        $cencelMoney = $orderInfo['earnest_money'] + $orderInfo['payment'];
        //更新我的余额
        $res1 = self::updateMoney($cencelMoney, $orderInfo['user_id']['id'],$order_id);
        //更新消费明细
        $res2 = self::updateSettlement(['user_id'=>$orderInfo['user_id']['id'], 'money'=>$cencelMoney, 'status'=>12, 'order_id'=>$order_id]);
        //修改订单状态
        model('Orders')->isUpdate(true)->save(['id'=>$order_id, 'cencel_money'=>$cencelMoney]);

        if($res1 && $res2){
            SendSms::send($orderInfo['user_id']['mobile'],[], 'cencel');
        }
    }

    //更新我的余额
    public static function updateMoney($money, $user_id, $order_id = '')
    {
        $save = [
            'id'=>$user_id,
            'money'=>Db::raw('money+'.$money)
        ];
        Db::startTrans();
        try {
            model('UsersS')->isUpdate(true)->save($save);
            Db::commit();
            return true;
        }catch (Exception $e){
            Db::rollback();
//            $save['order_id'] = $order_id;
            mylog(['desc'=>'更新我的余额失败','msg'=>$e->getMessage(),'data'=>$save,'lastSql'=>model('UsersS')->getlastsql()], 'sql');
        }
    }
    
    //新增消费明细
    public static function updateSettlement($data)
    {
//        status
//        1:购买用户vip；2:购买律师vip；3:付定金；4:付尾款；5:收定金；6:收尾款；
//        7:推荐注册；8:推荐验证律师；9:上传合同;10:提现申请;11:提现完成;12:撤消订单退款
        $save = [
            'user_id'=>$data['user_id']??'',
            'money'  =>$data['money']??'',
            'c_time' =>time(),
            'status' =>$data['status']??'',
            'get_time'=>$data['get_time']??'',
            'get_card_id'=>$data['get_card_id']??'',
            'trade_no'=>$data['trade_no']??'',
            'vipTag'=>$data['vipTag']??'',
            'source'=>$data['source']??'',
        ];
        Db::startTrans();
        try {
            model('SettlementDetails')->save($save);
            Db::commit();
            return true;
        }catch (Exception $e){
            Db::rollback();
            $save['order_id'] = $data['order_id'];
            mylog(['desc'=>'更新消费明细失败','msg'=>$e->getMessage(),'data'=>$save,'lastSql'=>model('SettlementDetails')->getlastsql()], 'sql');
        }
    }
}