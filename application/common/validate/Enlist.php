<?php
namespace app\common\validate;

use think\Validate;

class Enlist extends Validate{
    //规则
    protected $rule = [
        "money|报价"        =>  "require|float|>:0.01|token",
        "__token__"   =>  "require",
        "desc|描述"   =>  "require",
     ];

     protected $message =   [
         "id.token"      =>  "非法操作，不能重复提交",
     ];

     //场景
    protected $scene    =   [
    ];
}