<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * Class OrdersScore
 * 用户银行（三方）账号
 * @package app\common\model
 */
class Card extends Model{
    public $table = 'user_pay_card';
    public $pk = 'id';
    protected $autoWriteTimestamp=false;

}
