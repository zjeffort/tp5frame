function audiovideo_upload(userid,type,input){
    $.ajax({
        url: "/wap/Orders/ajaxQn",
        dataType: 'json',
        success: function (res) {
            var token = res.uptoken;
            var domain = res.domain;
            var config = {
                useCdnDomain: true,
                disableStatisticsReport: false,
                retryCount: 6,
                region: qiniu.region.z2
            };
            var putExtra = {
                fname: "",
                params: {},
                mimeType: null,
            };
            //执行文件绑定方法，并且监听点击上传
            input.unbind("change").bind("change", function () {
                //获取文件
                var filePath = $(this).val();
                //转换成文件名
                var pos = filePath.lastIndexOf("\\");
                var namefile = filePath.substring(pos + 1);
                //开始准备上传
                var file = this.files;
                file = file[0];
                // 点击开始上传
                if(type=="audio"){
                    var size=res.audioSize;
                }
                if(type=="video"){
                    var size=res.videoSize;
                }
                // 文件大小判断;
                if(fileCheck(file,size,type)){
                    var previewfile = $('<div class="preview'+type+'"><p>' + namefile + '</p><span>点击重新上传</span></div>');//预览图节点
                    $('.preview'+type+'').remove();
                    $('#add'+type+' img').hide();
                    $('#add'+type+'').append(previewfile);//添加预览视频名称
                    $('.'+type+'-progress').show(); 
                    uploadWithSDK(file, token, putExtra, config, domain,userid,type);
                }
            });
        }
    });
}