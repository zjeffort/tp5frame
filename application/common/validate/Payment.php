<?php
namespace app\common\validate;

use think\Validate;

class Payment extends Validate{

    //            'body'=>$params['body'],
//            'trade_no'=>$params['trade_no'],
//            'total_fee'=>$params['total_fee'],
//        ];

    //ali
//        $data = [
//            'data'=>trim($params['WIDbody']),                  //商品描述，可空
//            'subject'=>trim($params['WIDsubject']),            //订单名称，必填
//            'total_amount'=>trim($params['WIDtotal_amount']),  //付款金额，必填
//            'out_trade_no'=>trim($params['WIDout_trade_no']),  //商户订单号，商户网站订单系统中唯一订单号，必填
//        ];


    //规则
    protected $rule = [
//        "trade_type|支付类型"        =>  "require|token",
        "trade_type|支付类型"        =>  "require",
//        "__token__"   =>  "require",
        "body|商品描述"   =>  "require",
        //微信
        "trade_no|订单号"   =>  "require|alphaNum",
        "total_fee|支付金额"   =>  "require|>=:0.01",
        //支付宝
        "subject|订单名称"   =>  "require",
        "total_amount|付款金额"   =>  "require|>=:0.01",
        "out_trade_no|订单号"   =>  "require|alphaNum",
     ];

     protected $message =   [
//         "trade_type.token"      =>  "非法操作",
     ];

     //场景
    protected $scene    =   [
        'wxpay'=>[
            'body','trade_no','total_fee','trade_type'
        ],
        'alipay'=>[
            'data','subject','total_amount','out_trade_no','trade_type'
        ],
    ];
}