<?php
namespace app\index\controller;

use think\Controller;

class Base extends Controller{

    static public $userID = '';
    static public $userInfo = '';

    public function _initialize(){

        define('MODULE_NAME', request()->module());
        define('CONTROLLER_NAME', request()->controller());
        define('ACTION_NAME', request()->action());

        //获取站点配置信息
        $siteInfo = get_A_conf('site', 'site');
        $this->assign('siteInfo', $siteInfo);

        if(\think\Cookie::has('PHPSESSID') && session('sid') == cookie('PHPSESSID')){
            cookie('PHPSESSID', session_id(), 3600*24);  //用户只要在操作，cookie就永远不会过期
        }else{
            session('user', null);
        }

        self::$userInfo = session("user");
//        if(CONTROLLER_NAME == 'Index' && ACTION_NAME == 'index'){
//        }elseif(CONTROLLER_NAME == 'User' && ACTION_NAME == 'login' && request()->isPost()){
//        }else{
        if(in_array(CONTROLLER_NAME, ['Orders','Members'])){
            if ( empty(self::$userInfo) ) {
                $login = "<script>window.location.href = '/index/index.html?login=' + Date.parse(new Date()); </script>";
                echo $login;
                exit;
            }
        }

//        }

        $box_is_pjax = $this->request->isPjax();
        $this->assign('box_is_pjax', $box_is_pjax);

        self::$userID = session("user")['id'];
        $this->assign('userInfo', self::$userInfo);

    }

    //返回json
    protected  function returnJson($data=[], $err = 0, $msg = ''){
        $result = $this->returnArr($data,$err,$msg);
        return json($result);
    }

    //返回数组
    protected  function returnArr($data=[], $err = 0, $msg = ''){
        $result = [
            "err"   =>  $err,
            "msg"   =>  $this->getMsg($err, $msg),
            "data"  =>  $data,
        ];
        return $result;
    }

    public function getMsg($err,$msg){
        if($err){
            $msg = $msg ?? '失败';
        }else{
            $msg = '成功';
        }
        return $msg;
    }

    //获取用户id
//    public function getUserId(){
//        return session("user")['id'];
//    }
}
